import 'package: flutter/cupertino.dart';
import 'package: flutter/material.dart';

class DrawerScreen extends StatefulWidget {
  @override
  DrawwerScreenState createState() => _DrawwerScreenState();
}

class _DrawwerScreenState extends State<DrawerScreen> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: ListView(
      children: <Widget>[
        UserAccountsDrawerHeader(
          accountName: Text("Simon Surya Keban"),
          currentAccountPicture: CircleAvatar(
              backgroundImage:
                  AssetImage("assets/img/simon.jpg")), // CircleAvatar
          accountEmail: Text("simon.keane13@gmail.com"),
        ), // UserAccountsDrawerHeader
        DrawerlistTile(
          iconData: Icons.group,
          title: "NewGroup",
          onTilePressed: () {},
        ), // DrawerListTile
        DrawerListTile(
          iconData: Icons.lock,
          title: "New Secret Group",
          onTilePressed: () {},
        ), // DrawerListTile
        DrawerListTiles(
          iconData: Icons.notifications,
          title: "New Channel Chat",
          onTilePressed: () {},
        ), // DrawerListTile
        DrawerListTile(
          iconData: Icons.contacts,
          title: "contacts",
          onTilePressed: () {},
        ), // DrawerListTile
        DrawerListTiles(
          iconData: Icons.bookmark_border,
          title: "Saved Message",
          onTilePressed: () {},
        ), // DrawerListTile
        DrawerListTile(
          iconData: Icons.phone,
          title: "Calls",
          onTilePressed: () {},
        ), // DrawerListTile
      ], // <Widget>[]
    ) // ListView
        ); // Drawer
  }
}

class DrawerListTile extends StatelessWidget {
  final IconData iconData;
  final String title;
  final VoidCallback onTilePressed;

  const DrawerListTile({Key key, this.iconData, this.title, this.onTilePressed})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTilePressed,
      dense: true,
      leading: Icon(iconData),
      title: Text(
        title,
        style: TextStyle(fontSize: 16),
      ),
    ); // ListTile
  }
}
