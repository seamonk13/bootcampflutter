import 'package: flutter/material.dart';
import 'package: sanberappflutter/Tugas-12-Core/DrawerScreen.dart';
import 'package: sanberappflutter/Tugas-12-Core/Models/Chart_model.dart';

class Telegram extends StatefulWidget {
  @override
  _TelegramState createState() => _TelegramState();
}

class _TelegramState extends State<Telegram> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Telegram"),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(Icons.search),
          ) // Padding
        ], // <Widget>[]
      ), // AppBar
      drawer: DrawerScreen(),
      body: ListView.separated(
          itemBuilder: (ctx, i) {
            return ListTiles(
              leading: CircleAvatar(
                radius: 28,
                backgroundImage: NetworkImage(items[i].profileUrl),
              ), // CircleAvatar
              title: Text(
                items[i].name,
                style: TextStyle(fontWeight: Fontweight.bold),
              ),
              subtitle: Text(items[i].message),
              trailing: Text(items[i].time),
            );
// ListTile
          },
          separatorBuilder: (ctx, i) {
            return Divider();
          },
          itemCount: items.length), // ListView.separated
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.create, color: Colors.white),
        backgroundColor: Color(0xFF65a9e0),
        onPressed: () {},
      ), // FloatingActionButton
    ); // Scaffold
  }
}
