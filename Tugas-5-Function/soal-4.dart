void main() {
  print(findFactorial(6));
}


findFactorial(int no) {
  if (no <= 0) {
    return 1;
  }
  return no * findFactorial(no - 1);
}

