import 'bangun_datar.dart';

class Lingkaran extends BangunDatar {
//double nilai ;
double jari  ;
double  pi  ;

Lingkaran (double jari, double pi){

 // this.nilai= 3.14;
  this.jari=jari;
  this.pi=pi;
}
@override
double luas(){
return 3.14*jari*jari;
}
double keliling(){
return 2*pi*jari;
}

}