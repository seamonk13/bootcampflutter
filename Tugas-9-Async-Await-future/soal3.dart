
void main() async {
print("Ready..Sing");
print(await line());//3
print(await line2());//2
print(await line3());//1
print(await line4());
}

Future<String> line() async
{
  String sing="Pernahkah kau merasa";
  return await Future.delayed(Duration(seconds:5), ()=>(sing));
}

Future<String> line2() async
{
  String sing="Pernahkah kau merasa.......";
  return await Future.delayed(Duration(seconds:3), ()=>(sing));
}

Future<String> line3() async
{
  String sing="Pernahkah kau merasa";
  return await Future.delayed(Duration(seconds:2), ()=>(sing));
}

Future<String> line4() async
{
  String sing="Hatimu Hampa, pernahkah kau merasa hatimu Kosong....";
  return await Future.delayed(Duration(seconds:1), ()=>(sing));
}

